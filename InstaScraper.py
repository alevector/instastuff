import json
import requests
import re
import datetime
import gspread
import google.auth
from google.oauth2 import service_account

from urllib.request import urlopen
from oauth2client.service_account import ServiceAccountCredentials

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)

sheet=client.open("instastuff")
worksheet=sheet.get_worksheet(0)
worksheet.update_acell('E5', "testesttest")


#Open Instagram profile as json
profile_name="stocjia"
url= urlopen('https://www.instagram.com/'+profile_name+'/?__a=1').read().decode('utf8')
obj=json.loads(url)

current_followers=obj["user"]["followed_by"]
most_recent_post=obj["user"]["media"]["nodes"][0]
likes_most_recent= most_recent_post["likes"]
comments_most_recent=most_recent_post["comments"]
most_recent_date=most_recent_post["date"]

recent_date=datetime.datetime.fromtimestamp(
        int(str(most_recent_date))
    ).strftime('%Y-%m-%d %H:%M:%S')


#print(recent_date)
#print(json.dumps(current_followers,indent=4))
##print(json.dumps(most_recent_post,indent=4))
#print(json.dumps(likes_most_recent,indent=4))
#print(json.dumps(comments_most_recent,indent=4))

follower_num=re.findall('\d+',str(current_followers))
follower_num=float(follower_num[0])
recent_post_likes_num=re.findall('\d+',str(likes_most_recent))
recent_post_likes_num=float(recent_post_likes_num[0])
recent_post_comments_num=re.findall('\d+',str(comments_most_recent))
recent_post_comments_num=float(recent_post_comments_num[0])


#print(engagement)
